# Deep Matching and Validation Network
#### An End-to-End Solution to Constrained Image Splicing Localization and Detection

The provided **python2** (2.7.12) implementation of deep matching and validation network using the **Keras** deep neural network with the **Theano** backend. Detailed python lib dependencies are specified in `config/requirements.txt`.

## Directory Structure
- `config/` 
    - 'keras.json': a sample keras.json config with the Theano backend.
    - 'requirements.txt': a python2 lib env requirement file
- `data/` 
    - `paired_CASIA_ids.csv`: defines the paired CASIA2 dataset
    - `README.md`: step-by-step instruction to prepare CASIA2 dataset
    - `small/`: contains 40 small RGB images from CASIA2
- `expt/` 
    - `test_on_paired_casia/`: contains DMVN prediction results on the paired CASIA2 dataset
- `lib/` 
    - `dmvn/`: DMVN lib
- `model/` 
    - `dmvn_end_to_end.h5`: pretrained DMVN model
- `dmvn_example.ipynb`: ipython notebook of using DMVN to perform image splicing localization and detection using images from `data/small/`
- `dmvn_on_paired_casia.ipynb`: ipython notebook of testing DMVN performance on the paired CASIA2 dataset.
- `README.md`: the current file.

## Usage
Below is a simple code snippet of using the DMVN model to perform splicing localizaiton and detection on a pair of (probe, donor) images.

```python
# load DMVN model and image preprocess
from utils import preprocess_images
from core import create_DMVN_model

# create an end-to-end DMVN model
dmvn_end_to_end = create_DMVN_model()

# load two a DMVN sample of two images
Xp, Xd = preprocess_images( [ probe_file, donor_file ] )
X = { 'world' : Xd, 'probe' : Xp }

# splicing localization and detection via DMVN
pred_masks, pred_probs = dmvn_end_to_end.predict( X )
donor_mask, probe_mask = pred_masks[0]
splicing_prob = pred_probs.ravel()[1]
```

## Contact 
> Dr. Yue Wu

> Email: yue_wu@isi.edu

> Affiliation: USC Information Sciences Institute